package com.pryvarnikov.springhelloworld.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {
        private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

        @ExceptionHandler(Exception.class)
        @ResponseStatus(value = HttpStatus.BAD_REQUEST)
        @ResponseBody
        public Map<String, String> handleValidationException(Exception ex) {
            logger.warn("Handle validation exception '{}': {}", ex.getClass().getSimpleName(), ex.getMessage());
            Map<String, String> response = new HashMap<>(2);
            response.put("Exception", ex.getClass().getSimpleName());
            response.put("Message", ex.getMessage());
            return response;
        }
}
