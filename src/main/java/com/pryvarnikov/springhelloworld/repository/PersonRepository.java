package com.pryvarnikov.springhelloworld.repository;

import com.pryvarnikov.springhelloworld.model.PersonEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, Long> {
    @Modifying
    @Query(value = "update PersonEntity set " +
            "name = :name, " +
            "phone = :phone, " +
            "email = :email " +
            "where id = :id")
    int update(@Param("id") Long id,
               @Param("name") String name,
               @Param("phone") String phone,
               @Param("email") String email);
}
