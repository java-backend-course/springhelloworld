package com.pryvarnikov.springhelloworld.service;

import com.pryvarnikov.springhelloworld.model.PersonDto;
import com.pryvarnikov.springhelloworld.model.PersonEntity;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    Optional<PersonEntity> findById(Long id);
    List<PersonEntity> findAll();
    PersonEntity save(PersonDto personDto);
    boolean update(Long id, PersonDto personDto) throws Exception;
    boolean deleteById(Long id);
}
