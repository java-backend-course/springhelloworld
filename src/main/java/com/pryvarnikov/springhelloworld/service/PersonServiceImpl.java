package com.pryvarnikov.springhelloworld.service;

import com.pryvarnikov.springhelloworld.model.PersonDto;
import com.pryvarnikov.springhelloworld.model.PersonEntity;
import com.pryvarnikov.springhelloworld.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Optional<PersonEntity> findById(Long id) {
        return personRepository.findById(id);
    }

    @Override
    public List<PersonEntity> findAll() {
        return (List<PersonEntity>) personRepository.findAll();
    }

    @Override
    public PersonEntity save(PersonDto personDto) {
        PersonEntity personEntity = new PersonEntity(personDto.getName(), personDto.getPhone(), personDto.getEmail());
        return personRepository.save(personEntity);
    }

    @Override
    @Transactional
    public boolean update(Long id, PersonDto personDto) throws Exception {
        boolean operationResult = false;
        PersonEntity personEntity = personRepository.findById(id)
                .orElseThrow(() -> new Exception(String.format("Person with ID %d not found", id)));
        validatePersonDtoOnNull(personEntity, personDto);
        int updatedRows = personRepository.update(id, personDto.getName(), personDto.getPhone(), personDto.getEmail());
        if (updatedRows == 1) {
            operationResult = true;
        }
        return operationResult;
    }

    private void validatePersonDtoOnNull(PersonEntity personEntity, PersonDto personDto) {
        if (personDto.getName() == null) {
            personDto.setName(personEntity.getName());
        }
        if (personDto.getPhone() == null) {
            personDto.setPhone(personEntity.getPhone());
        }
        if (personDto.getEmail() == null) {
            personDto.setEmail(personEntity.getEmail());
        }
    }

    @Override
    public boolean deleteById(Long id) {
        boolean operationResult = false;

        if (personRepository.findById(id).isPresent()) {
            personRepository.deleteById(id);
            operationResult = true;
        }

        return operationResult;
    }

}
