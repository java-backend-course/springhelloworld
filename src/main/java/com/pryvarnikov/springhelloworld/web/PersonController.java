package com.pryvarnikov.springhelloworld.web;

import com.pryvarnikov.springhelloworld.model.PersonDto;
import com.pryvarnikov.springhelloworld.model.PersonEntity;
import com.pryvarnikov.springhelloworld.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/person", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    private final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @GetMapping("/get_all")
    public List<PersonEntity> getAll() {
        List<PersonEntity> persons = personService.findAll();
        logger.info("GET /get_all: {}", persons);
        return persons;
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable(value = "id") Long id, @RequestBody PersonDto personDto) throws Exception {
        boolean operationResult;
        operationResult = personService.update(id, personDto);

        if (operationResult) {
            logger.info("PUT /update/{} with data {}: Ok", id, personDto);
        } else {
            throw new Exception(String.format("Failed to update person with id %d", id));
        }
    }

    @PostMapping("/add")
    public PersonEntity addPerson(@RequestBody PersonDto personDto) {
        PersonEntity personEntity = personService.save(personDto);
        logger.info("POST /add/{}: {}", personDto, personEntity);
        return personEntity;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        boolean operationResult;
        operationResult = personService.deleteById(id);
        if (operationResult) {
            logger.info("Person with ID {} has been successfully deleted", id);
        } else {
            logger.warn("The person with ID {} has not been deleted", id);
        }
    }
}
