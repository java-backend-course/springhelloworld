create table persons
(
    id    bigint auto_increment not null,
    person_name  varchar(30),
    person_phone varchar(13),
    person_email varchar(60),
    primary key (id)
);